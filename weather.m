%MatLab applications in physics
%Author: Dawid Lazaj
%Technical Physics

%to run properly, there has to be 'FeelsLikeTemperature' function code in the same file as this script

clear;

%this block checks if .csv file is created
%if no, it creates it and make header, otherwise we skip to the next part
if(~exist('weather.csv'))
   fid = fopen('weather.csv', 'w');
   header = {'Source', 'Station', 'Date', 'Time', 'Temperature[Celsius]',...
             'FeelsLikeTemperature[Celsius]', 'Pressure[hPa]', 'Wind[km/h]', 'Humidity[%]'};
   fprintf(fid, '%s; %s; %s; %s; %s; %s; %s; %s; %s\n', header{:});
   fclose(fid);
end

%json is used to decode data from the sources
addpath('jsonlab-master');
stations = {'katowice', 'czestochowa', 'raciborz'};
cycles = 0;
cyclesInput = input('Specify how many cycles you want the program to run.');

%obtaining time from PC
%if minutes are correct, program loads data and writes it in .csv file
while(1);
  fprintf('Acquiring data...\n')
  measure = clock;
  if measure(5) == 45;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for a = 1:size(stations, 2);
      IMGW(a) = loadjson(urlread(cell2mat...
         (['https://danepubliczne.imgw.pl/api/data/synop/station/' stations(a) '/format/json'])));
      WTTR(a) = loadjson(urlread(cell2mat...
                                  (['http://wttr.in/' stations(a) '?format=j1'])));    
    end

    %from aqcuired and segregated data now we can easily get to values we're interested in 
    for b = 1:size(stations, 2);
      %imgw
      temperatureIMGW(b) = str2double(IMGW(b).temperatura);
      pressureIMGW(b) = str2double(IMGW(b).cisnienie);
      windIMGW(b) = str2double(IMGW(b).predkosc_wiatru)*3.6;
      humidityIMGW(b) = str2double(IMGW(b).wilgotnosc_wzgledna);
      timeIMGW{b} = [num2str(IMGW(b).godzina_pomiaru) ':00'];
      dateIMGW{b} = IMGW(b).data_pomiaru;
      feelsLikeTemperatureIMGW(b) = FeelsLikeTemperature(temperatureIMGW(b),...
                                    windIMGW(b), humidityIMGW(b)); 
      %wttr
      temperatureWTTR(b) = str2double(WTTR(b).current_condition{1}.temp_C);
      pressureWTTR(b) = str2double(WTTR(b).current_condition{1}.pressure);
      windWTTR(b) = str2double(WTTR(b).current_condition{1}.windspeedKmph);
      humidityWTTR(b) = str2double(WTTR(b).current_condition{1}.humidity);
      timeWTTR{b} = datestr(WTTR(b).current_condition{1}.observation_time, 15);
      dateWTTR{b} = datestr(WTTR(b).weather{1}.date, 29);
      feelsLikeTemperatureWTTR(b) = FeelsLikeTemperature(temperatureWTTR(b),...
                                    windWTTR(b), humidityWTTR(b));
    end

    %aqcuired data we can write now in created earlier file
    fid = fopen('weather.csv', 'a');
    for c = 1:size(stations, 2);
      fprintf(fid, 'IMGW; %s; %s; %s; %f; %f; %f; %f; %f\n', stations{c}, dateIMGW{c},...
      timeIMGW{c}, temperatureIMGW(c), feelsLikeTemperatureIMGW(c),...
      pressureIMGW(c), windIMGW(c), humidityIMGW(c));
    end 
    for d = 1:size(stations, 2);
      fprintf(fid, 'WTTR; %s; %s; %s; %f; %f; %f; %f; %f\n', stations{d}, dateWTTR{d},...
      timeWTTR{d}, temperatureWTTR(d), feelsLikeTemperatureWTTR(d),...
      pressureWTTR(d), windWTTR(d), humidityWTTR(d));
    end
    fclose(fid);

    %file is now ready
    %we can now look for extreme values
    fid = fopen('weather.csv', 'r');
    readFile = textscan(fid, '%s %s %s %s %f %f %f %f %f', 'Delimiter', ';', 'HeaderLines', 1);
    date = readFile{3};
    time = readFile{4};
    for e = 1:5;
      [MAX(e), maxINDEX(e)] = max(readFile{e+4});
      [MIN(e), minINDEX(e)] = min(readFile{e+4});
    end
    
    %everything is ready
    %program now has to present collected data to the user
    %next block prints in window extreme values and time of observing them
    fprintf('Program ended data acquiring process.\n\n')
    fprintf('Maximum / Minimum values of registered data:\n');
    fprintf('Temperature: %f/%f Celsius at %s/%s on %s/%s\n', MAX(1), MIN(1),...
             cell2mat(time(maxINDEX(1))), cell2mat(time(minINDEX(1))),...
             cell2mat(date(maxINDEX(1))), cell2mat(date(minINDEX(1))));
    fprintf('FeelsLikeTemperature: %f/%f Celsius at %s/%s on %s/%s\n', MAX(2), MIN(2),...
             cell2mat(time(maxINDEX(2))), cell2mat(time(minINDEX(2))),...
             cell2mat(date(maxINDEX(2))), cell2mat(date(minINDEX(2))));    
    fprintf('Pressure: %f/%f Celsius at %s/%s on %s/%s\n', MAX(3), MIN(3),...
             cell2mat(time(maxINDEX(3))), cell2mat(time(minINDEX(3))),...
             cell2mat(date(maxINDEX(3))), cell2mat(date(minINDEX(3))));    
    fprintf('Wind: %f/%f Celsius at %s/%s on %s/%s\n', MAX(4), MIN(4),...
             cell2mat(time(maxINDEX(4))), cell2mat(time(minINDEX(4))),...
             cell2mat(date(maxINDEX(4))), cell2mat(date(minINDEX(4))));
    fprintf('Humidity: %f/%f Celsius at %s/%s on %s/%s\n', MAX(5), MIN(5),...
             cell2mat(time(maxINDEX(5))), cell2mat(time(minINDEX(5))),...
             cell2mat(date(maxINDEX(5))), cell2mat(date(minINDEX(5))));
    cycles = cycles + 1;
    fclose(fid);
  end
  
  %let's explain why the majority of program is located in while loop
  %program takes from user information about quantity of cycles the program commits
  %in layman's terms it asks how many hours user wants the program to run
  %f.e. for 2 cycles program takes data from source first time it meets required minutes
  %then waits a minute so it doesn't repeat reading for the same hour
  %program will wait for an hour, take data again, do required operations specified in while loop and end it's work
  %if user wants the program to run endlessly he's supposed to give cyclesInput value 0
  if cyclesInput ~= 0
    if cycles == cyclesInput;
      break;
    end
  end
  pause(60);
end

