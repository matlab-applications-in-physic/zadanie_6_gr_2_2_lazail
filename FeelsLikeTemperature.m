%MatLab applications in physics
%Author: Dawid Lazaj
%Technical Physics

%function based on 'https://gist.github.com/jfcarr/e68593c92c878257550d'
%takes temperature in Celsius degrees, wind in km/h and humidity in %
%calculates Feels Like Temperature in Celsius degrees

function FLT = FeelsLikeTemperature(t, w, h)
      tF = t*1.8 + 32; %convert Celsius to Farenheit
      wMPH = w*0.62137; %convert km/h to miles/h
      %windchill
      if tF <=50 && wMPH >= 3
         FeelsLikeTemperature = (35.74 + (0.6215*tF) - 35.75*(wMPH^0.16)...
                                       + ((0.4275*tF)*(wMPH^0.16)));
      else
         FeelsLikeTemperature = tF;
      end
      %heat index
      if FeelsLikeTemperature == tF && tF >= 80.0
         FeelsLikeTemperature = 0.5*(tF + 61.0 + ((tF-68.0)*1.2) + (h*0.094));
         if FeelsLikeTemperature >= 80.0
            FeelsLikeTemperature = -42.379 + 2.04901523*tF + 10.14333127*h -...
            0.22475541*t*h - 0.00683783*t*t - 0.05481717*h*h + 0.00122874*t*t*h +...
            0.00085282*t*h*h - 0.00000199*t*t*h*h;
            if h < 13 && tF >= 80 && tF <= 112
               FeelsLikeTemperature = FeelsLikeTemperature - ((13 - h)/4)*sqrt((17-abs(tF-95.0))/17);
               if h > 85 && tF >= 80 && tF <= 87
                  FeelsLikeTemperature = FeelsLikeTemperature + ((h-85)/10)*((87-tF)/5);
               end
            end
         end
      end
      FLT = (FeelsLikeTemperature - 32)/1.8; %back to celsius
end